// True Partners, LLC licenses this file to you under the MIT license.

import UIKit

class JavascriptMessageMapper: NSObject {
    private var propertyMap = MessagePropertyMap();
    
    init (propertyMap map: MessagePropertyMap? = nil) {
        super.init()
        
        if map != nil {
            propertyMap = map!
        }
    }
    
    func fromDictionary(dictionary: [String: Any]) -> JavascriptMessage {
        let result = JavascriptMessage()
        
        result.name = dictionary[propertyMap.name] as! String?
        result.payload = dictionary[propertyMap.payload]as! [String: Any]?
        result.id = dictionary[propertyMap.id] as! String?
        result.rsvp = dictionary[propertyMap.rsvp] as! Bool? ?? false
        result.responseTo = dictionary[propertyMap.responseTo] as! String?
            
        return result;
    }
    
    func toJson(message: JavascriptMessage) -> String? {
        let dict = toDictionary(message: message)
        var result:String? = nil
        
        if JSONSerialization.isValidJSONObject(dict) {
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
                
                result = String(decoding: data, as:UTF8.self)
            }
            catch {
                print("\(error)")
            }
        }
        
        return result
    }
    
    private func toDictionary(message:JavascriptMessage) -> [String:Any] {
        var result = [String:Any]()
        
        if let idVal = message.id {
            result[propertyMap.id] = idVal
        }
        
        if let nameVal = message.name {
            result[propertyMap.name] = nameVal
        }
        
        if let dataVal = message.payload {
            result[propertyMap.payload] = dataVal
        }
        
        if let responseToVal = message.responseTo {
            result[propertyMap.responseTo] = responseToVal
        }
        
        result[propertyMap.rsvp] = message.rsvp
        
        return result;
    }
}
