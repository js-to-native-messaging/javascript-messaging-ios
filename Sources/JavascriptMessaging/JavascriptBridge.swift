// True Partners, LLC licenses this file to you under the MIT license.

import Foundation;
import WebKit;

public typealias JavascriptMessageHandler = (JavascriptMessage) throws -> Void;

public protocol JavascriptMessageReceiver : AnyObject {
    func didReceiveMessage(messageData: [String: Any])
}

public protocol JavascriptMessageDispatcher {
    func dispatchMessage(json: String, errorHandler: ((Error) -> Void)?)
}

public class JavascriptBridge: NSObject, JavascriptMessageReceiver {
    public var initialized = false
    private(set) public var name:String!
    private var messageCount = 0;
    private var defaultHandler:JavascriptMessageHandler!
    private var pendingMessages: [JavascriptMessage]?
    private var messageHandlers: [String: JavascriptMessageHandler]!
    private var callbackHandlers: [String:JavascriptMessageHandler]!
    private(set) public var initMessageName:String!
    private var dispatcher:JavascriptMessageDispatcher!
    private var messageMapper:JavascriptMessageMapper!;
    
    static public var messageReceivedNotificationName:NSNotification.Name {
        return NSNotification.Name("JavascriptBridgeMessageReceived")
    }
    static public var messageSendFailedNotificationName: NSNotification.Name {
        return NSNotification.Name("JavascriptBridgeMessageSendFailed")
    }
    
    public convenience init?(webView wv:WKWebView, name bridgeName: String = "nativeBridge", initMessageName messageName: String? = nil, initMessageProcessor: ((JavascriptMessage) -> [String: Any])? = nil, messagePropertyMap propertyMap: MessagePropertyMap? = nil) {
        
        let wvDispatcher = WebviewMessageDispatcher(webView: wv, contentControllerName: bridgeName)
        
        self.init(dispatcher: wvDispatcher, name: bridgeName, initMessageName: messageName, initMessageProcessor: initMessageProcessor, messagePropertyMap: propertyMap)
        
        wvDispatcher.receiver = self
    }
    
    init?(dispatcher d: JavascriptMessageDispatcher, name bridgeName: String = "nativeBridge", initMessageName messageName: String? = nil, initMessageProcessor: ((JavascriptMessage) -> [String: Any])? = nil, messagePropertyMap propertyMap: MessagePropertyMap? = nil) {
        if bridgeName.isEmpty {
            return nil
        }
        
        super.init();
        
        name = bridgeName
        initMessageName = messageName ?? "\(name!).init"
        messageMapper = JavascriptMessageMapper(propertyMap: propertyMap)
        dispatcher = d
        
        print("Initializing JavascriptBridge. \(name!), \(initMessageName!)")
        
        defaultHandler = {(message: JavascriptMessage) -> Void in
            DispatchQueue.main.async{
                NotificationCenter.default.post(name: JavascriptBridge.messageReceivedNotificationName, object: nil, userInfo: ["message": message])
            }
        }
        
        pendingMessages = [JavascriptMessage]()
        messageHandlers = [String: JavascriptMessageHandler]()
        callbackHandlers = [String:JavascriptMessageHandler]()
        
        // ensure the init message is handled
        messageHandlers[initMessageName] = {(message: JavascriptMessage) throws -> Void in
            var payload:[String:Any]?
            
            if let processor = initMessageProcessor {
                payload = processor(message)
            }
            
            self.initialized = true
            
            // be sure the init response is sent first
            try message.callback!(JavascriptMessage(name: self.initMessageName, payload: payload))
            
            self.flushPending()
        }
    }
    
    public func setHandler(messageName:String, handler:@escaping JavascriptMessageHandler) {
        if(messageName != initMessageName) {
            messageHandlers[messageName] = handler
        }
    }
    
    public func removeHandler(messageName: String) {
        if(messageName != initMessageName) {
            messageHandlers.removeValue(forKey: messageName)
        }
    }
    
    public func sendMessage(_ message:JavascriptMessage) {
        if !initialized {
            queueMessage(message: message)
        }
        else {
            dispatchMessage(message)
        }
    }
    
    public func sendMessage(name: String, payload: [String:Any]? = nil, callback:JavascriptMessageHandler? = nil) {
        let message = JavascriptMessage(name: name, payload: payload, callback: callback)
        
        sendMessage(message)
    }
    
    public func didReceiveMessage(messageData: [String : Any]) {
        let message = messageMapper.fromDictionary(dictionary: messageData)
        
        print("Processing \(String(describing: message.name))")
        
        if let responseId = message.responseTo, let callback = callbackHandlers[responseId] {
            do {
                try callback(message)
            }
            catch {
                broadcastError(message, error)
            }
            
            callbackHandlers.removeValue(forKey: responseId)
        }
        else {
            var handler:JavascriptMessageHandler?
            
            if let messageName = message.name {
                handler = messageHandlers[messageName]
            }
            
            if handler == nil {
                handler = defaultHandler
            }

            if let messageId = message.id, message.rsvp && !messageId.isEmpty {
                message.callback = { (msg:JavascriptMessage) -> Void in
                    msg.responseTo = messageId
                    
                    self.sendMessage(msg)
                }
            }
            
            do {
                try handler!(message)
            } catch {
                if let messageCallback = message.callback {
                    do {
                        try  messageCallback(JavascriptMessage(name: message.name, payload: ["error": error.localizedDescription], callback: nil))
                    } catch {
                        print("error callback error: \(error)")
                    }
                }
                broadcastError(message, error)
            }
        }
    }
    
    private func queueMessage(message: JavascriptMessage) {
        pendingMessages?.append(message)
    }
    
    func flushPending() {
        if let messages = pendingMessages, initialized {
            for message in messages {
                dispatchMessage(message)
            }
        }
        
        pendingMessages = nil
    }
    
    private func dispatchMessage(_ message: JavascriptMessage) {
        messageCount += 1;
        
        message.id = "\(self.name!)_\(messageCount)"
        
        if let messageId = message.id, let callback = message.callback {
            callbackHandlers[messageId] = callback
        }
        
        let json = messageMapper.toJson(message: message)
        
        if let jsonVal = json {
            dispatcher?.dispatchMessage(json: jsonVal, errorHandler: { (error: Error) in
                if message.retries < 3 {
                    message.retries += 1
                    self.callbackHandlers.removeValue(forKey: message.id!)
                    self.dispatchMessage(message)
                }
                else {
                    NotificationCenter.default.post(name: JavascriptBridge.messageSendFailedNotificationName, object: nil, userInfo: ["message": message, "error": error ])
                }
            })
        }
    }
    
    private func broadcastError(_ message:JavascriptMessage, _ error: Error) {
        DispatchQueue.main.async{
            NotificationCenter.default.post(name: JavascriptBridge.messageSendFailedNotificationName, object: nil, userInfo: ["message": message, "error": error])
        }
    }
    
    deinit {
        dispatcher = nil
        pendingMessages = nil
        callbackHandlers = nil
        messageHandlers = nil
        defaultHandler = nil
    }
    
}
