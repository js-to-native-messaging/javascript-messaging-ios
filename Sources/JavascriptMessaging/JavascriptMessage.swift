// True Partners, LLC licenses this file to you under the MIT license.

import UIKit

public class JavascriptMessage: NSObject {
    public var id:String?
    public var responseTo:String?
    public var rsvp = false
    public var name:String?
    public var payload:[String: Any]?
    public var callback:JavascriptMessageHandler? {
        didSet {
            rsvp = callback != nil
        }
    }
    public var retries = 0
    
    public override init() {
        name = ""
        super.init()
    }
    
    public convenience init(name:String?, payload: [String: Any]?, callback:JavascriptMessageHandler? = nil) {
        self.init()
        
        self.name = name
        self.payload = payload
        self.callback = callback;
    }
}
