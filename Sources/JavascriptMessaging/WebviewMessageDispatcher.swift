// True Partners, LLC licenses this file to you under the MIT license.
import UIKit;
import WebKit;

class WebviewMessageDispatcher: NSObject, JavascriptMessageDispatcher, WKScriptMessageHandler {
    private weak var webView: WKWebView!
    weak var receiver: JavascriptMessageReceiver!
    private(set) var contentControllerName:String!
    
    init(webView wv: WKWebView, contentControllerName name: String = "nativeBridge", receiver r: JavascriptMessageReceiver? = nil) {
        super.init()
        
        webView = wv
        receiver = r
        contentControllerName = name
        
        webView.configuration.userContentController.add(self, name: name)
    }
    
    public func dispatchMessage(json: String, errorHandler: ((Error) -> Void)?) {
        let script = "\(self.contentControllerName!)_processWebviewMessage(\(json));"
            
        DispatchQueue.main.async {
            self.webView!.evaluateJavaScript(script) { result, error in
                if let errorVal = error {
                   errorHandler?(errorVal)
                }
            }
        }
    }
    
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        receiver?.didReceiveMessage(messageData: message.body as! [String : Any])
    }
}
