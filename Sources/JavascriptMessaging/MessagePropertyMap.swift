// True Partners, LLC licenses this file to you under the MIT license.

public struct MessagePropertyMap {
    public var id = "id"
    public var name = "name"
    public var payload = "payload"
    public var rsvp = "rsvp"
    public var responseTo = "responseTo"
    
    public init() {
        
    }
}
