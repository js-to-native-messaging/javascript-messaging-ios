import XCTest
@testable import JavascriptMessaging

final class JavascriptMessagingTests: XCTestCase {

    func testMessageCreation() {
        let message = JavascriptMessage(name: "test.message", payload: ["key": "value"])
        
        XCTAssertEqual(message.name, "test.message")
        XCTAssertFalse(message.payload == nil)
        XCTAssertEqual(message.payload!["key"] as! String, "value")
    }

    static var allTests = [
        ("testMessageCreation", testMessageCreation),
    ]
}
