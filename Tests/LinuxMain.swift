import XCTest

import JavascriptMessagingTests

var tests = [XCTestCaseEntry]()
tests += JavascriptMessagingTests.allTests()
XCTMain(tests)
